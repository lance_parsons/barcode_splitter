#!/usr/bin/env python
'''
Created on May 31, 2012

@author: rleach, lparsons
'''

from setuptools import setup
from codecs import open
from os import path
import os
import re


# The following (modified) method was obtained from:
# https://github.com/pypa/pip/blob/1.5.6/setup.py#L33
def read(*names, **kwargs):
    '''Reads a file such as a python script.  Used by find_version.'''
    return open(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                             *names), 'r').read()


# The following method was obtained from:
# https://packaging.python.org/single_source_version/#single-sourcing-the-version
def find_version(*file_paths):
    '''Finds a version number among the supplied files'''
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__\s*=\s*['\"]?([^'\"]*)['\"]?",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


# Versions should adhere to [Semantic Versioning](http://semver.org/).
VERSION = find_version('barcode_splitter.py')

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup_args = dict(
    name="barcode_splitter",
    version=VERSION,

    description="A utility to split multiple sequence files using multiple "
                "sets of barcodes",
    long_description=long_description,

    # The project's main homepage.
    url="https://bitbucket.org/princeton_genomics/barcode_splitter",

    # Author details
    author="Robert Leach, Lance Parsons",
    author_email="rleach@princeton.edu, lparsons@princeton.edu",

    license="BSD 2-clause",

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 4 - Beta',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Bio-Informatics',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: BSD License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.1',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],

    # What does your project relate to?
    keywords="sequencing fastq paired barcode demultiplex",

    py_modules=['barcode_splitter'],

    extras_require={
        'dev': ['check-manifest', 'flake8'],
        'test': ['tox'],
    },

    install_requires=[
        'ordereddict>=1.1;python_version<"2.7"',
        'argparse;python_version<"2.7"'
    ],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'barcode_splitter=barcode_splitter:main',
        ],
    },

)

setup(**setup_args)
