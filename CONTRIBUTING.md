Guidelines for developers
=========================

1.  Code must follow [PEP-8 standards](https://www.python.org/dev/peps/pep-0008/)
2.  All pipelines tests must pass before pull requests will be merged
3.  New functionality must have a corresponding test(s) created
4.  Keep the `CHANGELOG.md` up to date
5.  This project adheres to [Semantic Versioning](http://semver.org/).


Steps to contribute
-------------------

1.  [Fork the repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
2.  [Enable Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) on your fork
3.  [Clone your fork](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) on your development machine
4.  [Configure an upstream remote](https://help.github.com/articles/configuring-a-remote-for-a-fork/)
5.  [Checkout a new branch](https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html)
6.  [Make your changes](https://confluence.atlassian.com/bitbucket/work-on-local-source-files-223217905.html)
7.  Test your changes locally (see *Running tests* below)
8.  [Push your branch back up to your repository](https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html) (`git push origin <feature_branch>`)
9.  Ensure pipelines build successfully and your branch can be merged without conflict. If necessary, synchronize your fork
10. [Create a pull request](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html)

### Synchronize your fork

1.  Be sure you have [configured an upstream remote](https://help.github.com/articles/configuring-a-remote-for-a-fork/)
2.  [Fetch and merge upstream changes](https://help.github.com/articles/syncing-a-fork/)


Running tests
-------------

You can run the basic functional tests on your development machine by changing to the `tests` directory and running `./run_tests.sh`

We also use [Tox](https://tox.readthedocs.io/en/latest/) to test against multiple python versions. You can run tox tests locally if you have installed tox on your machine.

If you want to better simulate the pipelines tests (and get many python versions installed quickly), you can run the tests using Tox in a docker container.

1.  [Install Docker](https://www.docker.com/products/overview) on your machine

2.  Start up the container:
    ```
    docker run -it --volume="`pwd`:/barcode_splitter" --workdir="/barcode_splitter" --memory=4g --entrypoint=/bin/bash n42org/tox
    ```

3.  You can now run commands listed in the `bitbucket_pipeline.yml` file (e.g. `tox` to run tests on multiple python versions)


Releases
--------

1.  Update `__version__` in `barcode_splitter.py`.

2.  Update `CHANGELOG.md` from `Unreleased` to `<version>`, update diff links.

3.  Merge all PRs, ensure pipelines pass. Be sure to update local repo with
    merge commit (e.g. `git checkout master; git fetch upstream master; git
        merge upstream master`)

4.  Tag with new version:
    ```
    git tag -fa <version>
    git push origin --tags
    git push upstream --tags
    ```

5.  Create universal wheel:
    ```
    python setup.py bdist_wheel
    ```

6.  Register package and/or update metadata (README.rst, etc.):
    ```
    twine register dist/barcode_splitter-<version>-py2.py3-none-any.whl
    ```

7.  Upload new version of package:
    ```
    twine upload dist/barcode_splitter-<version>-py2.py3-none-any.whl
    ```

8.  Prepare for new development (can be pushed directly to master):

    1.  Change `__version__` in `barcode_splitter.py` to next version, appending
    `.dev1`.

    2.  Update `CHNAGELOG.md` to add `Unreleased` section, add diff link:
        ```
        https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/HEAD%0D<version>
        ```
